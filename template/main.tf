terraform {
  required_version = ">= 0.12.26"

  required_providers {
    google = {
      source  = "hashicorp/google"
      version = "~> 3.43.0"
    }
    google-beta = {
      source  = "hashicorp/google-beta"
      version = "~> 3.43.0"
    }
  }
}

provider "google" {
  project = var.project
  region  = var.region
}

provider "google-beta" {
  project = var.project
  region  = var.region
}


module "gke_cluster" {
  source = "../modules/gke-module"

  count = var.cluster_type == "public" ? 1 : 0 # Create the module instance conditionally

  name = var.cluster_name

  project  = var.project
  location = var.location
  network  = module.vpc_network.network

  subnetwork                    = module.vpc_network.public_subnetwork
  cluster_secondary_range_name  = module.vpc_network.public_subnetwork_secondary_range_name
  services_secondary_range_name = module.vpc_network.public_services_secondary_range_name

  alternative_default_service_account = var.override_default_node_pool_service_account ? module.gke_service_account.email : null

  enable_vertical_pod_autoscaling = var.enable_vertical_pod_autoscaling
  enable_workload_identity        = var.enable_workload_identity

  resource_labels = {
    environment = "testing"
  }
}

module "private_gke_cluster" {
  source = "../modules/gke-module"

  count = var.cluster_type == "private" ? 1 : 0 # Create the module instance conditionally

  name = var.cluster_name

  project  = var.project
  location = var.location
  network  = module.vpc_network.network

  subnetwork                    = module.vpc_network.public_subnetwork
  cluster_secondary_range_name  = module.vpc_network.public_subnetwork_secondary_range_name
  services_secondary_range_name = module.vpc_network.public_services_secondary_range_name

  master_ipv4_cidr_block = var.master_ipv4_cidr_block

  enable_private_nodes = "true"

  disable_public_endpoint = "false"

  master_authorized_networks_config = [
    {
      cidr_blocks = [
        {
          cidr_block   = "0.0.0.0/0"
          display_name = "all-for-testing"
        },
      ]
    },
  ]

  enable_vertical_pod_autoscaling = var.enable_vertical_pod_autoscaling
  enable_workload_identity        = var.enable_workload_identity

  resource_labels = {
    environment = "testing"
  }
}

resource "google_container_node_pool" "node_pool" {
  provider = google-beta
  name     = "main-pool"
  project  = var.project
  location = var.location
  cluster  = var.cluster_type == "public" ? module.gke_cluster[0].name : module.private_gke_cluster[0].name
  # cluster = var.cluster_value  # Use the variable here
  initial_node_count = "1"

  autoscaling {
    min_node_count = "1"
    max_node_count = var.cluster_type == "public" ? "1" : "1" # Adjust max node count based on cluster_type
  }

  management {
    auto_repair  = "true"
    auto_upgrade = "true"
  }

  node_config {
    image_type   = "COS_CONTAINERD"
    machine_type = "n1-standard-2"

    labels = {
      all-pools-example = "true"
    }

    disk_size_gb = "10"
    disk_type    = "pd-standard"
    preemptible  = false

    service_account = module.gke_service_account.email

    workload_metadata_config {
      node_metadata = "GKE_METADATA_SERVER"
    }

    oauth_scopes = [
      "https://www.googleapis.com/auth/cloud-platform",
    ]
  }

  timeouts {
    create = "30m"
    update = "30m"
    delete = "30m"
  }
}


module "gke_service_account" {
  source = "../modules/service-account-module"

  name        = var.cluster_service_account_name
  project     = var.project
  description = var.cluster_service_account_description
}

module "vpc_network" {
  source = "../modules/vpc-network-module"

  name_prefix = "${var.cluster_name}-network-${random_string.suffix.result}"
  project     = var.project
  region      = var.region

  cidr_block           = var.vpc_cidr_block
  secondary_cidr_block = var.vpc_secondary_cidr_block

  public_subnetwork_secondary_range_name = var.public_subnetwork_secondary_range_name
  public_services_secondary_range_name   = var.public_services_secondary_range_name
  public_services_secondary_cidr_block   = var.public_services_secondary_cidr_block
  private_services_secondary_cidr_block  = var.private_services_secondary_cidr_block
  secondary_cidr_subnetwork_width_delta  = var.secondary_cidr_subnetwork_width_delta
  secondary_cidr_subnetwork_spacing      = var.secondary_cidr_subnetwork_spacing
}

resource "random_string" "suffix" {
  length  = 4
  special = false
  upper   = false
}

data "google_client_config" "default" {}

data "template_file" "kubeconfig" {
  template = file("${path.module}/kubeconfig.tpl")

  vars = {
    name           = var.cluster_name
    endpoint       = var.cluster_type == "public" ? module.gke_cluster[0].endpoint : module.private_gke_cluster[0].endpoint
    cluster_ca_certificate = var.cluster_type == "public" ? module.gke_cluster[0].cluster_ca_certificate : module.private_gke_cluster[0].cluster_ca_certificate
    access_token           = data.google_client_config.default.access_token
  }
}

resource "local_sensitive_file" "kubeconfig" {
  content  = data.template_file.kubeconfig.rendered
  filename = "kubeconfig"
}



